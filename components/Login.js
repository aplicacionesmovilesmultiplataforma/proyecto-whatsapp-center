import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { context } from '../App';

import UserService from "../services/UserService";

export const Login = () => {
  const {
    handleLogin,
    user,
    setUser
  } = context();

  const [state, setState] = useState({
    user: 'administrador',
    pass: '1234',
  });

  const [isError, setIsError] = useState(false)

  const handleChangeInput = (item, value) => {
    setState({ ...state, [item]: value });
  };
  const handleOnPress = () => {
    UserService.login(state).then(result => {
            if (result.status === 200) {
                handleLogin()
                setUser(result.data)
            } else {
                setIsError(true);
            }
        }).catch(error => {
            setIsError(true);
        });
  };

  return (
    <View style={styles.loginContainer}>
      <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/logo.png')} />

        <View style={styles.containerInput}>
          <TextInput
            style={styles.inputStylesLogin}
            onChangeText={(value) => handleChangeInput('user', value)}
            value={state.user}
          />
        </View>
        <View style={styles.containerInput}>
          <TextInput
            secureTextEntry={true}
            style={styles.inputStylesLogin}
            onChangeText={(value) => handleChangeInput('pass', value)}
            value={state.pass}
          />
        </View>
      </View>
      <View style={styles.containerBtn}>
        <TouchableOpacity style={styles.btnLogin} onPress={handleOnPress}>
          <Text style={styles.textSizeBtn}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  loginContainer: {
    width: 'auto',
    flex: 1,
    backgroundColor: '#11161C',
  },
  container: {
    marginTop: 120,
  },
  textSize: {
    marginBottom: 20,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
  inputStylesLogin: {
    height: 50,
    width: 300,
    backgroundColor: '#1A202A',
    color: '#fff',
    fontSize: 17,
    padding: 8,
    borderRadius: 10,
    borderColor: '#fff',
  },
  containerInput: {
    margin: 10,
    alignItems: 'center',
  },
  containerBtn: {
    marginTop: 30,
    alignItems: 'center',
  },
  textSizeBtn: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  btnLogin: {
    width: 200,
    height: 50,
    backgroundColor: '#4DD8C1',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  logo: {
    width: 150,
    height: 150,
    alignSelf: 'center',
  },
  containerImg: {
    alignItems: 'center',
  },
});
