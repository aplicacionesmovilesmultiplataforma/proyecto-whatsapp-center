import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import MessageService from '../services/MessageService';

export const ChatContact = ({ route }) => {
  const [item, setItem] = useState({});
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState('');
  const [items, setItems] = useState(0);
  const contact = route.params.contact;
  
  useEffect(() => {
    actualizarChat();
  });

  const actualizarChat = () => {
    return MessageService.listar(contact.idcontacto)
      .then((result) => {
        if (result.status === 200) {
          setMessages(result.data);
        } else {
          setMessages([]);
        }
      })
      .catch((error) => {
        setMessages([]);
      });
  };

  const handleChange = (value) => {
    setMessage(value);
  };

  const change_date = (numero) => {
    let unix_timestamp = numero
    const date_full = new Date(unix_timestamp * 1000);
    const year = date_full.getFullYear();
    const month = "0" + (date_full.getMonth() + 1);
    const date = "0" + date_full.getDate();
    const hours = "0" + date_full.getHours();
    const minutes = "0" + date_full.getMinutes();
    const formattedTimeHM = hours.substr(-2) + ':' + minutes.substr(-2) ;
    const formatedDate = date.substr(-2) + '/' + month.substr(-2) + '/' + year;
    return [formattedTimeHM, formatedDate];
}

  const handleOnPress = () => {
    MessageService.enviarMensaje(contact.idcontacto, message)
      .then((result) => {
        if (result.status === 200) {
          setMessage('');
          actualizarChat();
        } else {
          console.log(result);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const Message = ({ message, index }) => {
    return (
      <View>
        {index == 0 || ( change_date(messages[index-1].time)[1] != change_date(message.time)[1])
        ? <Text style={styles.date}>{change_date(message.time)[1]}</Text>
        :<View></View>}
        <View style={message.fromMe ? styles.chatSend : styles.chatRecived}>
          <Text>{message.body}</Text>
          <Text style={styles.hour}>{change_date(message.time)[0]}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.flatList}
        data={messages}
        renderItem={({ item, index }) => <Message message={item} index={index} />}
        keyExtractor={(item) => item.idcontacto}
      />
      <View style={styles.newMessage}>
        <TextInput
          style={styles.newMessageInput}
          onChangeText={(value) => handleChange(value)}
          value={message}></TextInput>
        <TouchableOpacity style={styles.btnMessage} onPress={handleOnPress}>
          <Text style={styles.textSizeBtn}>Enviar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    display: 'flex',
    flex: 1,
    flexWrap: 'wrap',
  },
  chatSend: {
    width: '80%',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginVertical: 3,
    backgroundColor: '#4DD8C1',
    alignSelf: 'flex-end',
  },
  chatRecived: {
    width: '80%',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginVertical: 3,
    backgroundColor: 'white',
    alignSelf: 'flex-start',
  },
  newMessage: {
    height: 25,
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    flex: 0.1
  },
  flatList:{
    flex: 0.9
  },
  newMessageInput: {
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 13.5,
    width: '80%',
  },
  btnMessage: {
    backgroundColor: '#11161C',
    padding: 5,
    borderRadius: 5,
    width: '18%',
    marginLeft: '2%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSizeBtn: {
    fontWeight: 'bold',
    color: 'white',
  },
  date:{
    backgroundColor: '#e1e1e1',
    alignSelf: 'center',
    padding: 3,
    fontSize: 12,
    borderRadius: 3,
    marginBottom: 3,
    marginTop: 5,
  },
  hour:{
    fontSize: 10,
    alignSelf: 'flex-end',
    color: '#727272'
  },
});