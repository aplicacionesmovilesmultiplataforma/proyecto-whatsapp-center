import { SERVIDOR } from './Servidor';
import axios from 'axios';

class MenssageService {

    static urlLista = SERVIDOR + '/mensaje/lista/';
    static urlEnviarMensaje = SERVIDOR + '/mensaje/enviar/';
    static urlEliminarMensaje=SERVIDOR+'/mensaje/eliminar/'


    static listar = (idcontacto) => {
        return axios.get(this.urlLista+idcontacto);
    }

    static enviarMensaje = (idcontacto,message) => {
        let enviarData = {
            idcontacto: idcontacto,
            mensaje:message
        };

        return axios.post(this.urlEnviarMensaje,enviarData);
    }

    static eliminarMensaje = (idcontacto,idmensaje) => {
        return axios.delete(this.urlEliminarMensaje+idcontacto+"/"+idmensaje);
    }


}

export default MenssageService;