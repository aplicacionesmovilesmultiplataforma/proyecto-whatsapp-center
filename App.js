import React, { useState, createContext, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Login } from './components/Login';
import { AsignedContacts } from './components/AsignedContacts';
import { ChatContact } from './components/ChatContact';
import { Alert, Text, TouchableOpacity, StyleSheet } from 'react-native';

const Stack = createStackNavigator();
const AuthContext = createContext();
export const context = () => useContext(AuthContext);

export default function App() {
  const [login, setlogin] = useState(false);
  const [user, setUser] = useState(false);
  const handleLogin = () => setlogin(true);
  const handleLogout = () => {
    Alert.alert(
      'Logout',
      '¿Estas seguro que quieres salir?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('cancelado'),
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: () => setlogin(false),
        },
      ],
      { cancelable: false }
    );
  };
  const options = {
    headerStyle: {
      backgroundColor: '#008080',
      fontFamily: 'Helvetica',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: '700',
    },
  };
  return (
    <AuthContext.Provider value={{ user, setUser, handleLogin }}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: { backgroundColor: '#1A202A' },
            headerTintColor: '#FFF',
          }}>
          {login ? (
            <>
              <Stack.Screen
                name="AsignedContacts"
                component={AsignedContacts}
                options={{
                  ...options,
                  headerRight: () => (
                    <TouchableOpacity onPress={() => handleLogout()} style={styles.btnSalir}>
                      <Text style={{ color: '#079DCF' }}>Salir</Text>
                    </TouchableOpacity>
                  ),
                  title: 'Contactos Asignados'
                }}
              />

              <Stack.Screen name="ChatContact" 
                component={ChatContact}
                
                options={({ route }) => ({ ...options,title: route.params.contact.nombre })} />
            </>
          ) : (
            <>
              <Stack.Screen
                name="Login"
                component={Login}
                options={{ headerShown: false }}
              />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({
  btnSalir: {
    backgroundColor: 'white',
    color: '#11161C',
    marginRight: 10,
    padding: 5,
    width: 50,
    borderRadius: 5,
    alignItems:'center',
    justifyContent: 'center'
  }
});
