import { SERVIDOR } from './Servidor';
import axios from 'axios';

class UserService {

    static urlLogin = SERVIDOR + '/usuario/login';
    static urlRegistrar = SERVIDOR + '/usuario/registrar';
    static urlActualizar = SERVIDOR + '/usuario/actualizar';
    static urlEliminar = SERVIDOR + '/usuario/eliminar';

    static login = (data) => {
        let enviarData = {
            usuario: data.user,
            clave: data.pass
        };
        return axios.post(this.urlLogin, enviarData);
    }

    static registrar = (data) => {
        let enviarData = {
            idempresa: data.idempresa,
            apellidos:data.apellidos,
            nombres:data.nombres,
            idrol:data.idrol,
            usuario:data.usuario,
            clave:data.clave
        };
        return axios.post(this.urlRegistrar, enviarData);
    }

    static actualizar = (data,idusuario) => {
        let enviarData = {
            idusuario: idusuario,
            idempresa: data.idempresa,
            apellidos:data.apellidos,
            nombres:data.nombres,
            idrol:data.idrol,
            usuario:data.usuario,
            clave:data.clave
        };
        return axios.put(this.urlActualizar, enviarData);
    }

    static eliminar = (idusuario) => {
        return axios.delete(this.urlEliminar+"/"+idusuario);
    }
}



export default UserService;