import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import ContactService from '../services/ContactService';
import ContactNumberService from '../services/ContactNumberService';
import { context } from '../App';

export const AsignedContacts = ({ navigation }) => {
  const [contacts, setContacts] = useState([]);
  const [idNumero, setIdNumero] = useState(0);
  const [idEmpresa, setIdEmpresa] = useState(0);
  const { user } = context();
  useEffect(() => {
    ContactNumberService.listar(user.idempresa)
      .then((result) => {
        if (result.status === 200) {
          setIdNumero(result.data[0].idnumero);
        } else {
          setIdNumero(0);
        }
      })
      .catch((error) => {
        setIdNumero(0);
      });
  }, [user.idempresa]);

  useEffect(() => {
    ContactService.listar(idNumero, user.idusuario)
      .then((result) => {
        if (result.status === 200) {
          setContacts(result.data);
        } else {
          setContacts([]);
        }
      })
      .catch((error) => {
        setContacts([]);
      });
  }, [idNumero, user.idusuario]);

  const handleItem = (contact) => {
    navigation.navigate('ChatContact', { contact: contact });
  };
  const change_date = (string) => { //2021-01-06 06:40
    const date_full = string.split(' ');
    const date = date_full[0].split('-');
    const hour = date_full[1];
    const format_date = date[2] + "/" + date[1] + "/" + date[0];
    const result = format_date + " " + hour
    return result;
}
  const Contact = ({ contact }) => {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => handleItem(contact)}>
        <View style={styles.contianerImage}>
          <Image source={{ uri: contact.imagen }} style={styles.image} />
        </View>
        <View>
          <View style={styles.containerTitle}>
            <Text style={styles.title}>{contact.nombre}</Text>
          </View>
          <View style={styles.containerDescripcion}>
            <Text style={styles.descripcion}>{change_date(contact.fconexion)}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View>
      <FlatList
        data={contacts}
        renderItem={({ item }) => <Contact contact={item} />}
        keyExtractor={(item) => item.idcontacto}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    borderBottomWidth: 0.6,
    borderBottomColor: '#595959',
    padding: 5,
    marginVertical: 3,
    marginHorizontal: 7,
    flex: 1,
    flexDirection: 'row',
  },
  contianerImage: {
    borderColor: 'black',
    marginRight: 20,
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },

  containerTitle: {
    width: 230,
    height: 25,
    overflow: 'hidden',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#464545',
  },
  descripcion: {},
  containerDescripcion: {
    width: 240,
    height: 40,
    overflow: 'hidden',
  },
});